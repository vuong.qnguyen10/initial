Clone the source code
Create .env file in the root of project and copy content below
```
CLASS_FORMAT=full
NODE_PATH=src/
```

cd into the project folder
Run
```
yarn install
```
Then run
```
yarn start
```
Webpage is served at address http://localhost:3001