import React from 'react';

import styles from './styles.css';

const Index = () => (
  <div className={styles.main}>
    © 2018 teto.com. All Rights Reserved
  </div>
);

export default Index;
