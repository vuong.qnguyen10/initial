import React from 'react';
import T from 'prop-types';
import { Link } from 'react-router';

import styles from './styles.css';

const menu = [
  {
    text: 'Ảnh hài',
    url: '/image',
    active: true,
  },
  {
    text: 'Video hài',
    url: '/video',
    active: false,
  },
  {
    text: 'Linh tinh',
    url: '/image',
    active: false,
  },
  {
    text: 'Liên hệ',
    url: '/contact',
    active: true,
  },
];

const Menu1 = props => (
  <div className={styles.item}>
    <Link className={styles.item} to={`${props.url}`}>{props.text}</Link>
  </div>
);

Menu1.propTypes = {
  text: T.string.isRequired,
  url: T.string.isRequired,
};

const Menu2 = () => (
  <div className={styles.item}>
    <Link className={styles.item} to="/upload">Up ảnh</Link>
  </div>
);

const Index = () => (
  <div>
    <div>
      {menu.map(i => i.active && <Menu1 {...i} key={i.text} />)}
    </div>
    <div className={styles.menu2}>
      <Menu2 />
    </div>
  </div>
);

export default Index;
