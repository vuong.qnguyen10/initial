import T from 'prop-types';
import React from 'react';

import Head from './Head';
import Navigation from './Navigation';
import Footer from './Footer';
import styles from './global.css';

const App = props => (<div className={`${styles.variable} ${styles.main}`}>
  <div className={styles.head}>
    <Head />
  </div>
  <div className={styles.body}>
    <div className={styles.left}>
      {props.children}
    </div>
    <div className={styles.right}>
      <Navigation />
    </div>
  </div>
  <div className={styles.footer}>
    <Footer />
  </div>
</div>);

App.propTypes = {
  children: T.node.isRequired,
};

export default App;
