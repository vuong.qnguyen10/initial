import React from 'react';

import styles from './styles.css';

const Index = () => (
  <div className={styles.background}>
    <p>Liên hệ: mọi chi tiết xin vui lòng liên hệ qua Email:
      <strong>vuong.qnguyen10@gmail.com</strong>
    </p>
    <p>Chân thành cảm ơn các bạn đã ủng hộ TeTo ! :)</p>
    <p>Mọi vấn đề về bản quyền xin liên hệ qua phía trên :) !</p>
  </div>
);

export default Index;
