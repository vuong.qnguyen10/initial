import { connect } from 'react-redux';

import { load, love, hate } from 'action/post';
import Container from './Container';

const mapStateToProps = state => ({
  post: state.post,
});

const mapDispatchToProps = dispatch => ({
  load: id => dispatch(load(id)),
  love: () => dispatch(love()),
  hate: () => dispatch(hate()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
