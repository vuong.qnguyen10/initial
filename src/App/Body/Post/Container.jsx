import T from 'prop-types';
import React from 'react';

import View from './View';

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.love = this.love.bind(this);
    this.hate = this.hate.bind(this);
  }

  componentDidMount() {
    this.props.load(this.props.params.id);
  }

  componentWillUnmount() {
    this.props.params.id = '';
  }

  love() {
    this.props.love(this.props.post);
  }

  hate() {
    this.props.hate(this.props.post);
  }

  render() {
    return (<View
      id={this.props.params.id}
      {...this.props.post}
      love={this.love}
      hate={this.hate}
    />);
  }
}

Container.propTypes = {
  load: T.func.isRequired,
  params: T.shape({
    id: T.string,
  }).isRequired,
  post: T.shape({}).isRequired,
  love: T.func.isRequired,
  hate: T.func.isRequired,
};

export default Container;
