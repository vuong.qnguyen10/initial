import T from 'prop-types';
import React from 'react';

import Item from './Item';

const View = props => <Item id={props.id} post={props.post} love={props.love} hate={props.hate} />;

View.propTypes = {
  post: T.shape({}).isRequired,
  love: T.func.isRequired,
  hate: T.func.isRequired,
  id: T.string.isRequired,
};

View.defaultProps = {
  post: {},
};

export default View;
