import React from 'react';
import { Link } from 'react-router';

import Comment from 'components/Facebook/Comment';

import styles from './styles.css';

/* eslint-disable */
const View = props => props.post.objectId ?
  <div className={styles.main}>
    <div className={styles.title}>
      <Link to={`/post/${props.post.objectId}`} className={styles.link}>
        {props.post.title}
      </Link>
    </div>
    <Link to={`/post/${props.post.objectId}`} className={styles.link}>
      <img src={props.post.image} alt={props.post.objectId} className={styles.img} />
    </Link>
    <div className={styles.action}>
      <span className={styles.item} onClick={props.love}>
        {props.post.love === 0 ? '' : props.post.love} 👍
      </span>
      <span className={styles.item} onClick={props.hate}>
        {props.post.hate === 0 ? '' : props.post.hate} 👎
      </span>
    </div>
    <div className={styles.comment}>
      <Comment id={props.id} />
    </div>
  </div> : <div>Loading</div>;

export default View;
