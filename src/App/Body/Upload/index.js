import { connect } from 'react-redux';

import { upload } from 'action/content';
import Container from './Container';

const mapStateToProps = state => ({
  content: state.content,
});

const mapDispatchToProps = dispatch => ({
  upload: data => dispatch(upload(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
