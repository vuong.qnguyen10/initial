import T from 'prop-types';
import React from 'react';

import Input from 'components/Input';
import File from 'components/File';
import Button from 'components/Button';

import styles from './styles.css';

const View = ({ setForm, form, submit, success }) => (<div className={styles.background}>
  {success === true && <div>Đăng tải thành công!</div>}
  {success === false && <div>Đăng tải thất bại!</div>}
  <div className={styles.item}>
    <Input onChange={setForm.title} placeholder="Tiêu đề" value={form.title} />
  </div>
  <div className={styles.item}>
    <File onChange={setForm.image} preview value={form.image} />
  </div>
  <div className={styles.item}>
    <Button onClick={submit}>Tải lên</Button>
  </div>
</div>);

View.propTypes = {
  setForm: T.shape({
    title: T.func,
    image: T.func,
  }).isRequired,
  submit: T.func.isRequired,
  form: T.shape({
    title: T.string,
    image: T.File,
  }).isRequired,
  success: T.bool,
};

View.defaultProps = {
  success: undefined,
};

export default View;
