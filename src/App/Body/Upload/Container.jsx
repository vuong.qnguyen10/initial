import T from 'prop-types';
import React from 'react';

import View from './View';

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      form: {
        title: '',
        image: null,
      },
    };

    this.setForm = Object
      .keys(this.state.form)
      .reduce((prev, key) => ({
        ...prev,
        [key]: this.setFormWKey.bind(this, key),
      }), {});
  }

  componentWillReceiveProps(current) {
    if (current.content.success) {
      this.setState({
        form: {
          title: '',
          image: null,
        },
      });
    }
  }

  setFormWKey(key, value) {
    const nextForm = { ...this.state.form, [key]: value };
    this.setState({ form: nextForm, error: '' });
  }

  submit() {
    this.props.upload(this.state.form);
  }

  render() {
    return (<View
      {...this.state}
      setForm={this.setForm}
      submit={this.submit}
      {...this.props.content}
    />);
  }
}

Container.propTypes = {
  upload: T.func.isRequired,
  content: T.shape({
    success: T.bool,
  }).isRequired,
};

export default Container;
