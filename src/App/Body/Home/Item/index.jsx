import React from 'react';
import { Link } from 'react-router';

import Comment from 'components/Facebook/Comment';

import styles from './styles.css';

/* eslint-disable */
const View = props => props.objectId ?
  <div className={styles.main}>
    <div className={styles.title}>
      <Link to={`/post/${props.objectId}`} className={styles.link}>
        {props.title}
      </Link>
    </div>
    <Link to={`/post/${props.objectId}`} className={styles.link}>
      <img src={props.image} alt={props.objectId} className={styles.img} />
    </Link>
    <div className={styles.action}>
      <span className={styles.item} onClick={() => props.like(props.objectId)}>
        {props.love === 0 ? '' : props.love} 👍
      </span>
      <span className={styles.item} onClick={() => props.dislike(props.objectId)}>
        {props.hate === 0 ? '' : props.hate} 👎
      </span>
    </div>
    <div className={styles.comment}>
      <Comment id={props.objectId} />
    </div>
  </div> : <div>Loading</div>;

export default View;
