import T from 'prop-types';
import React from 'react';

import Item from './Item';

const View = ({ content, ...others }) =>
  content.map((item, index) => <Item {...item} key={index} {...others} />);

View.propTypes = {
  content: T.arrayOf(T.object).isRequired,
};

View.defaultProps = {
  content: [],
};

export default View;
