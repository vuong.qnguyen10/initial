import T from 'prop-types';
import React from 'react';

import View from './View';

class Container extends React.Component {
  constructor(props) {
    super(props);
    this.like = this.like.bind(this);
    this.dislike = this.dislike.bind(this);
  }
  componentWillMount() {
    this.props.load();
  }

  like(id) {
    this.props.love(id);
  }

  dislike(id) {
    this.props.hate(id);
  }

  render() {
    return <View {...this.props.content} like={this.like} dislike={this.dislike} />;
  }
}

Container.propTypes = {
  load: T.func.isRequired,
  love: T.func.isRequired,
  hate: T.func.isRequired,
  content: T.shape([]).isRequired,
};

export default Container;
