import { connect } from 'react-redux';

import { load, love, hate } from 'action/content';
import Container from './Container';

const mapStateToProps = state => ({
  content: state.content,
});

const mapDispatchToProps = dispatch => ({
  load: () => dispatch(load()),
  love: id => dispatch(love(id)),
  hate: id => dispatch(hate(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
