import { connect } from 'react-redux';

import { enter } from 'action/search';

import Container from './Container';

// const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  enter: text => dispatch(enter(text)),
});

export default connect(null, mapDispatchToProps)(Container);
