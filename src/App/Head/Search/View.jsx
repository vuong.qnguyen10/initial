import React from 'react';
import Input from 'components/Input';

import styles from './styles.css';

const View = () => (
  <div className={styles.container}>
    <Input />
  </div>
);

export default View;
