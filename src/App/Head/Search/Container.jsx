import React from 'react';
import T from 'prop-types';

import View from './View';

class Container extends React.Component {
  enter() {
    this.props.enter();
  }

  render() {
    return <View />;
  }
}

Container.propTypes = {
  enter: T.func.isRequired,
};

Container.defaultProps = {
  enter: () => { },
};

export default Container;
