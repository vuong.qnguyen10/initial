import React from 'react';

import Search from './Search';

import styles from './styles.css';

const Index = () => (
  <div className={styles.main}>
    <Search />
  </div>
);

export default Index;
