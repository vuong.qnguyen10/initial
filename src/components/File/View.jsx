import T from 'prop-types';
import React from 'react';

import Preview from './Preview';

const FileInputView = ({ id, onChange, required, preview, value }) => (
  <div>
    {/* <div className={`${styles.main} ${border ? styles.border : ''}`}> */}
    <input
      id={id} onChange={onChange}
      required={required} type="file"
    />
    {preview && (
      <Preview id={id} value={value} />
    )}
    {/* <label className={styles.label} htmlFor={id}>
      <span className={styles.button}>
        {translate(`${value ? 'change' : 'choose'}`)}
      </span>
      <span className={styles.name}>
        {value ? value.name : ''}
      </span>
    </label> */}
  </div>
);

FileInputView.propTypes = {
  id: T.string.isRequired,
  value: T.shape({}),
  onChange: T.func.isRequired,
  required: T.bool,
  preview: T.bool,
};

FileInputView.defaultProps = {
  id: 'file',
  preview: false,
};

export default FileInputView;
