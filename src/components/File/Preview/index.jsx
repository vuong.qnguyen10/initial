import T from 'prop-types';
import React from 'react';

import styles from './styles.css';

const Index = ({ id, value }) => (
  <div className={styles.main}>
    {value ? (
      <img
        id={`${id}--preview`} alt={value.name}
        src={window.URL.createObjectURL(value)}
        className={styles.img}
      />
    ) : (<div className={styles.empty}>
      Xem thử
      </div>
      )}
  </div>
);

Index.propTypes = {
  id: T.string.isRequired,
  value: T.shape({}),
};

Index.defaultProps = {
  id: '',
};

export default Index;
