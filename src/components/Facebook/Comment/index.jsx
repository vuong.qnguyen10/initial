import T from 'prop-types';
import React from 'react';

import View from './View';

/* eslint-disable */
const fbSDK = (d, s, id) => {
  let js;
  const fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s);
  js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}

class Comment extends React.Component {
  componentWillMount() {
    fbSDK(document, 'script', 'facebook-jssdk');
  }

  componentDidMount() {
    if (window.FB && window.FB.XFBML) {
      window.FB.XFBML.parse();
    }
  }

  render() {
    return (
      <View {...this.props} />
    );
  }
}

Comment.propTypes = {
  id: T.string.isRequired,
};

export default Comment;
