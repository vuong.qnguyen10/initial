import T from 'prop-types';
import React from 'react';

const View = ({ id }) => <div className="fb-comments" data-href={`${location.origin}/${id}`} data-numposts="5" />;

View.propTypes = {
  id: T.string.isRequired,
};

export default View;
