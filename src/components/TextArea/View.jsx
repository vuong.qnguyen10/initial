import T from 'prop-types';
import React from 'react';

import styles from './styles.css';

const View = ({ id, className, ...others }) => (
  <textarea
    className={`${className} ${styles.main}`}
    id={`${id}-input`}
    {...others}
  />
);

View.defaultProps = {
  id: '',
  className: '',
  rows: 5,
};

View.propTypes = {
  className: T.string,
  id: T.string,
  row: T.number,
};

export default View;
