import T from 'prop-types';
import React from 'react';

import styles from './styles.css';

const Button = ({ type, className, children, ...other }) => (
  <button type={type}
    className={` ${className} ${styles.button}`}
    {...other}
  >
    {children}
  </button>
);

Button.propTypes = {
  type: T.string,
  children: T.node.isRequired,
  className: T.string,
};

Button.defaultProps = {
  type: 'button',
  className: '',
};

export default Button;
