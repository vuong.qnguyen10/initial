import T from 'prop-types';
import React from 'react';

import styles from './styles.css';

const View = ({ id, className, type, border, value, ...others }) => (
  <input
    className={`${className} ${styles.main} ${border ? styles.border : ''}`}
    type={type}
    id={id}
    value={value}
    {...others}
  />
);

View.defaultProps = {
  type: 'text',
  id: '',
  className: '',
  value: '',
};

View.propTypes = {
  type: T.string,
  className: T.string,
  border: T.bool,
  id: T.string,
  value: T.string,
};

export default View;
