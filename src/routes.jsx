import React from 'react';
import { Router, Route, browserHistory, IndexRedirect, IndexRoute } from 'react-router';

import App from 'App';
import Home from 'App/Body/Home';
import Contact from 'App/Body/Contact';
import Post from 'App/Body/Post';
import Upload from 'App/Body/Upload';

const Routes = () => {
  return (
    <Router history={browserHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="/post/:id" component={Post} />
        <Route path="/contact" component={Contact} />
        <Route path="/upload" component={Upload} />
      </Route>
      <Route path="*">
        <IndexRedirect to="/" />
      </Route>
    </Router>
  );
};

export default Routes;
