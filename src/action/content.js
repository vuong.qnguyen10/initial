import { get, post, update } from 'service/content';

export const load = () => {
  return async dispatch => {
    const data = await get();
    dispatch({
      type: 'GET_CONTENT',
      data,
    });
  };
};

export const love = id => {
  return async (dispatch, getState) => {
    let item;
    const data = getState().content.content.map(i => {
      if (i.objectId === id) {
        item = {
          ...i,
          love: i.love + 1,
        };
        return item;
      }
      return i;
    });
    await update(item);
    dispatch({
      type: 'GET_CONTENT',
      data,
    });
  };
};

export const hate = id => {
  return async (dispatch, getState) => {
    let item;
    const data = getState().content.content.map(i => {
      if (i.objectId === id) {
        item = {
          ...i,
          hate: i.hate + 1,
        };
        return item;
      }
      return i;
    });
    await update(item);
    dispatch({
      type: 'GET_CONTENT',
      data,
    });
  };
};

export const upload = data => {
  return async dispatch => {
    try {
      await post(data);
      dispatch({
        type: 'UPLOAD',
      });
    } catch (error) {
      dispatch({
        type: 'UPLOAD_FALSE',
      });
    }
  };
};
