// todo: get single item
export const $get = async (url) => {
  const res = await fetch(`/${url}`, {
    method: 'get',
  });
  const body = await res.json();
  return body;
};

export const $post = async (url, data) => {
  const res = await fetch(`${url}`, {
    headers: {
      // 'Content-Type': 'multipart/form-data',
      // 'Content-Type': 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoding',
    },
    method: 'post',
    body: data,
  });
  const body = await res.json();
  return body;
};

export const $put = async (url, data) => {
  const res = await fetch(`${url}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'put',
    body: JSON.stringify(data),
  });
  const body = await res.json();
  return body;
};

// todo: delete goes here
