import { get, update } from 'service/content';

export const load = id => {
  return async dispatch => {
    const data = await get(id);
    dispatch({
      type: 'GET_POST',
      post: data[0],
    });
  };
};

export const love = () => {
  return async (dispatch, getState) => {
    const post = {
      ...getState().post.post,
      love: getState().post.post.love + 1,
    };
    await update(post);
    dispatch({
      type: 'GET_POST',
      post,
    });
  };
};

export const hate = () => {
  return async (dispatch, getState) => {
    const post = {
      ...getState().post.post,
      hate: getState().post.post.hate + 1,
    };
    await update(post);
    dispatch({
      type: 'GET_POST',
      post,
    });
  };
};
