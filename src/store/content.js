const initialState = {
  success: undefined,
};

const contentReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CONTENT':
      return {
        ...state,
        content: action.data,
      };
    case 'UPLOAD':
      return {
        ...state,
        success: true,
      };
    case 'UPLOAD_FALSE':
      return {
        ...state,
        success: false,
      };
    default:
      return state;
  }
};

export default contentReducer;
