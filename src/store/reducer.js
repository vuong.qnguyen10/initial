import { combineReducers } from 'redux';

import content from './content';
import post from './post';

export default combineReducers({
  content,
  post,
});
