const initialState = {};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_POST':
      return {
        ...state,
        post: action.post,
      };
    default:
      return state;
  }
};

export default postReducer;
