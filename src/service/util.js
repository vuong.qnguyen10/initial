export const wrap = async func => {
  const result = await new Promise((resolve, reject) => {
    func.then(data => resolve(data))
      .catch(data => reject(data));
  });
  return result;
};
