import Backendless from './backendless';
import { wrap } from './util';

const Content = Backendless.Data.of('content');
const query = Backendless.DataQueryBuilder.create();

export const get = async (id = '') => {
  if (id) {
    query.setWhereClause(`objectId='${id}'`);
  } else {
    query.setWhereClause('');
  }

  const result = await wrap(Content.find(query));
  return result;
};

export const post = async content => {
  const blob = content.image.slice(0, content.image.size, 'image/png');
  const extension = /[^.]*$/.exec(content.image.name)[0];
  const file = new File([blob], `${new Date().getTime().toString()}.${extension}`, { type: 'image/png' });

  const url = await wrap(Backendless.Files.upload(file, '/Image'));
  const result = await wrap(Content.save({
    title: content.title,
    image: url.fileURL,
  }));

  return result;
};


export const update = async data => {
  await wrap(Content.save(data));
};
